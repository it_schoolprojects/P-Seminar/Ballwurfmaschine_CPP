#pragma once

namespace core {

	class algorithms
	{
		static double calc_angle_friction_newton(double x, double y, double y_0, double v_0, double m, double profile, double c_w, double density, int precision, bool alternative);
		static void check_not_a_number(double phi_approx);
		static void check_loop(double phi_approx, double phi_approx_prev, double precision_loop_counter);
		static bool height_reachable(double y, double y_0, double v_0, double m, double profile, double c_w, double density);
	public:
		static double calc_angle_friction_algorithm(double x, double y, double y_0, double v_0, double m, double profile, double c_w, double density, int precision);
	};

}