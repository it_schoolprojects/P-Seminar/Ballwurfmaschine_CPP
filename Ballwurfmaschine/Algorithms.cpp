#include "Algorithms.h"
#include "Functions.h"
#include <cmath>
#include "not_a_number_exception.h"
#include "loop_exception.h"
#include "point_not_reachable_exception.h"
#include "angle_not_useable_exception.h"
#include <iostream>

double core::algorithms::calc_angle_friction_newton(const double x, const double y, const double y_0, const double v_0,
                                                    const double m, const double profile, const double c_w,
                                                    const double density, const int precision,
                                                    const bool alternative)
{
	auto phi_approx = functions::calc_angle_idealized(x, y, y_0, v_0, alternative);
	//Startwert ist der Winkel unter idealisierten Bedingungen
	check_not_a_number(phi_approx);
	auto precision_loop_counter = 0;
	int current_precision;


	{
		const auto y_approx = functions::calc_trajectory_friction(x, phi_approx, y_0, v_0, m, profile, c_w, density);
		current_precision = int(log10(1 / abs(y_approx - y)));
	}

	while (current_precision < precision)
	{
		//Solange die gew�nschte Pr�zision nicht erreicht ist, mache weiter
		const auto prev_precision = current_precision;
		const auto phi_approx_prev = phi_approx;
		//Newton-Algorithm
		const auto y_approx = functions::calc_trajectory_friction(x, phi_approx, y_0, v_0, m, profile, c_w, density);
		phi_approx -= (y_approx - y) / (functions::calc_trajectory_friction_derived_phi(
			x, phi_approx, v_0, m, profile, c_w, density)); // phi_n+1 = phi_n - y(phi_n)/y'(phi_n)
		//Checks + logging
		current_precision = int(log10(1 / abs(y_approx - y)));
		if (current_precision <= prev_precision)
			precision_loop_counter++;
		else
			precision_loop_counter = 0;
		check_not_a_number(phi_approx);
		check_loop(phi_approx, phi_approx_prev, precision_loop_counter);
		std::cout << phi_approx << " : " << y_approx << std::endl;
	}

	if (phi_approx > 90 || phi_approx < 0)
		throw angle_not_useable_exception();
	return phi_approx;
}

void core::algorithms::check_not_a_number(const double phi_approx)
{
	if (isinf(phi_approx))
		throw not_a_number_exception(not_a_number_exception::infinite);
	if (isnan(phi_approx))
		throw not_a_number_exception(not_a_number_exception::nan);
}

void core::algorithms::check_loop(const double phi_approx, const double phi_approx_prev,
                                  const double precision_loop_counter)
{
	if (phi_approx_prev == phi_approx || phi_approx > 500 || phi_approx < -500 || precision_loop_counter > 500)
		throw loop_exception();
}

bool core::algorithms::height_reachable(const double y, const double y_0, const double v_0, const double m,
                                        const double profile, const double c_w, const double density)
{
	const double phi_max = 90;
	const auto phi_max_rad = pi / 2; // PI / 2 = 90�
	const auto c = functions::calc_c(profile, c_w, density, m);
	const auto t_u = functions::calc_t_u(phi_max_rad, v_0, c);
	const auto y_max = functions::calc_height_friction(t_u, phi_max, y_0, v_0, m, profile, c_w, density);
	return y <= y_max;
}

double core::algorithms::calc_angle_friction_algorithm(const double x, const double y, const double y_0,
                                                       const double v_0,
                                                       const double m, const double profile, const double c_w,
                                                       const double density, const int precision)
{
	try
	{
		return calc_angle_friction_newton(x, y, y_0, v_0, m, profile, c_w, density, precision, false);
	}
	catch (const not_a_number_exception& e)
	{
		if (e.get_state() == not_a_number_exception::infinite && height_reachable(y, y_0, v_0, m, profile, c_w, density))
			return 90;
		throw point_not_reachable_exception("Calculation failed. Point cannot be aimed.");
	} catch (const loop_exception&)
	{
		try
		{
			return calc_angle_friction_newton(x, y, y_0, v_0, m, profile, c_w, density, precision, true);
		}
		catch (const not_a_number_exception& a)
		{
			if (a.get_state() == not_a_number_exception::infinite && height_reachable(y, y_0, v_0, m, profile, c_w, density))
				return 90;
			throw point_not_reachable_exception("Calculation failed. Point cannot be aimed.");
		}
		catch (const loop_exception&)
		{
			throw point_not_reachable_exception("Calculation failed. Point cannot be aimed.");
		}
		catch (const angle_not_useable_exception&)
		{
			throw point_not_reachable_exception("Calculation failed. Point cannot be aimed.");
		}
	}
	catch (const angle_not_useable_exception&)
	{
		try
		{
			return calc_angle_friction_newton(x, y, y_0, v_0, m, profile, c_w, density, precision, true);
		}
		catch (const not_a_number_exception& a)
		{
			if (a.get_state() == not_a_number_exception::infinite && height_reachable(y, y_0, v_0, m, profile, c_w, density))
				return 90;
			throw point_not_reachable_exception("Calculation failed. Point cannot be aimed.");
		}
		catch (const loop_exception&)
		{
			throw point_not_reachable_exception("Calculation failed. Point cannot be aimed.");
		}
		catch (const angle_not_useable_exception&)
		{
			throw point_not_reachable_exception("Calculation failed. Point cannot be aimed.");
		}
	}
}
