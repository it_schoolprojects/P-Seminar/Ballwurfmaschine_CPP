#pragma once
#include <exception>

namespace core {
	class not_a_number_exception : public std::exception
	{
	public:
		enum number_state
		{
			infinite,
			nan
		};
		explicit not_a_number_exception(number_state state);
		number_state get_state() const;
	private:
		number_state state_;
	};
}