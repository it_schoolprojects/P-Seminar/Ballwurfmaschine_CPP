#pragma once
#include "Algorithms.h"

namespace core {
	const double pi = 3.1415926535897932384626433832795;
	const double g = 9.81;
	class functions
	{
		friend class algorithms;
		static double to_degrees(double rad);
		static double to_radians(double deg);
		static double calc_trajectory_friction_derived_phi(double x, double phi, double v_0, double m, double profile, double c_w, double density);
		static double calc_x_u(double phi_rad, double v_0, double c);
		static double calc_t_u(double phi_rad, double v_0, double c);
		static double calc_c(double profile, double c_w, double density, double m);


	public:
		static double calc_angle_idealized(double x, double y, double y_0, double v_0, bool alternative);
		static double calc_trajectory_friction(double x, double phi, double y_0, double v_0, double m, double profile, double c_w, double density);
		static double calc_height_friction(double t, double phi, double y_0, double v_0, double m, double profile, double c_w, double density);
	};
}
