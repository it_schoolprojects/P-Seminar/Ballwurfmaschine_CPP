#include "stdafx.h"
#include "CppUnitTest.h"
#include "../Ballwurfmaschine/Algorithms.h"
#include "../Ballwurfmaschine/point_not_reachable_exception.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace UnitTest
{
	extern const double m = 0.0575;
	extern const double profile = 0.014;
	extern const double c_w = 0.45;
	extern const double density = 1.2041;

	TEST_CLASS(UnitTest1)
	{
		//PUBLIC
	public:

		TEST_METHOD(testAngleCalculationVerticalReachable)
		{
			const auto calculated = core::algorithms::calc_angle_friction_algorithm(0, 5, 0, 15, m, profile, c_w, density, 8);
			Assert::IsTrue(doubleEquals(90, calculated), std::to_wstring(calculated).c_str());
		}

		TEST_METHOD(testAngleCalculationVerticalNotReachable)
		{
			try
			{
				const auto calculated = core::algorithms::calc_angle_friction_algorithm(0, 5, 0, 10, m, profile, c_w, density, 8);
				Assert::IsTrue(false, std::to_wstring(calculated).c_str());
			} catch (core::point_not_reachable_exception)
			{
				//This Exception is expected. Test passed.
			}
		}

		TEST_METHOD(testAngleCalculationNotVerticalReachable)
		{
			const auto calculated = core::algorithms::calc_angle_friction_algorithm(
				11.41, 3.13, 1.5, 15, m, profile, c_w, density, 8);
			Assert::IsTrue(doubleEquals(39, calculated), std::to_wstring(calculated).c_str());
		}

		TEST_METHOD(testAngleCalculationNotVerticalNotReachable)
		{
			try
			{
				const auto calculated = core::algorithms::calc_angle_friction_algorithm(12, 3, 1.5, 15, m, profile, c_w, density, 8);
				Assert::IsTrue(false, std::to_wstring(calculated).c_str());
			}
			catch(core::point_not_reachable_exception&)
			{
			}
		}

		//PRIVATE
	private:
		static bool doubleEquals(const double d1, const double d2)
		{
			return abs(d1 - d2) < 0.1;
		}
	};
}
